//Uncomment the following line if you are compiling this code in Visual Studio
//#include "stdafx.h"

#include <opencv2/opencv.hpp>
// => /usr/local/Cellar/opencv/3.4.3/include/opencv2
#include <iostream>
#include "AronLib.h" 
// => $HOME/myfile/bitbucket/cpplib/AronLib.h"
using namespace cv;
using namespace std;
using namespace Utility;

/*
-------------------------------------------------------------------------------- 
Mon Nov 26 09:26:42 2018 
-------------------------------------------------------------------------------- 
Image Blending
Compile: runopencv ImageBlending.cpp => ImageBlending 
Script:  /Users/cat/myfile/bitbucket/script/runopencv.sh
ref: http://localhost/html/indexOpenCVLinearInterpolation.html
--------------------------------------------------------------------------------
*/

Mat blendImage(Mat s1, float alpha, Mat s2, float beta){
// Rect rect;
// rect.x = 0;
// rect.y = 0;
// rect.width = dst.size().width;;
// rect.height= dst.size().height;

//  Mat image(600, 800, CV_8UC3, Scalar(100, 200, 30)); 
    Mat dst;
    dst.create(s2.rows, s2.cols, CV_8UC3);
    for(int r=0; r<s1.rows; r++){
        for(int c=0; c<s1.cols; c++){
            Vec3b pt0 = s1.at<Vec3b>(Point(c, r));
            Vec3b pt1 = s2.at<Vec3b>(Point(c, r));
            Vec3b pt;
            pt[0] = (1 - alpha)*pt[0] + alpha*pt1[0];
            pt[1] = (1 - alpha)*pt[1] + alpha*pt1[1];
            pt[2] = (1 - alpha)*pt[2] + alpha*pt1[2];
            dst.at<Vec3b>(Point(c, r)) = pt; 
        }
    }
    return dst;
}
int main(int argc, char** argv) {
 // Read the image file

 // Mat image(600, 800, CV_8UC3, Scalar(100, 200, 30)); 
    Mat dogImage1 = imread("/Users/cat/try/dog3.jpeg");
    Mat dogImage2 = imread("/Users/cat/try/dog2.jpeg");
    int cropWidth = 200;
    int cropHeight= 200;

    pp("Original Img Width =%d   rows=[%d] cols=[%d]", dogImage1.size().width, dogImage1.rows, dogImage1.cols);
    pp("Original Img Height=%d\n", dogImage1.size().height);
    pp("New Img Width =%d\n", cropWidth);
    pp("New Img Height=%d\n", cropHeight);

    Rect roi1;
    int offX1 = (dogImage1.size().width  - cropWidth)/2;
    int offY1 = (dogImage1.size().height - cropHeight)/2;
    roi1.x = offX1;
    roi1.y = offY1;
    roi1.width  = cropWidth; 
    roi1.height = cropHeight; 

    Rect roi2;
    int offX2 = (dogImage2.size().width  - cropWidth)/2;
    int offY2 = (dogImage2.size().height - cropHeight)/2;
    roi2.x = offX2;
    roi2.y = offY2;
    roi2.width  = cropWidth; 
    roi2.height = cropHeight; 

    Mat dogCrop1 = dogImage1(roi1);
    Mat dogCrop2 = dogImage2(roi2);

    // Mat oriImg(600, 800, CV_8UC3, Scalar(100, 200, 30)); 
    
    float alpha = 0.5;
    float beta = 1 - alpha;
    // g(x) = (1 - alpha)f(x) + alpha h(x)
    Mat dstImg;
    addWeighted(dogCrop1, alpha, dogCrop2, beta, 0.0, dstImg);
    //Mat dstImg = blendImage(dogCrop2, alpha, dogCrop1, beta);


    String dogName1 = "dog name 1"; //Name of the window
    String dogName2 = "dog name 2"; //Name of the window
    String dogName3 = "Blending Dog"; //Name of the window

    String imgPath1 = "/Users/cat/try/bigdogcrop1.jpeg";
    String imgPath2 = "/Users/cat/try/bigdogcrop2.jpeg";
    String imgPath3 = "/Users/cat/try/blendingImg.jpeg";

    imwrite(imgPath1, dogCrop1);
    imwrite(imgPath2, dogCrop2);
    imwrite(imgPath3, dstImg);

    imshow(dogName1, dogCrop1); // Show our image inside the created window.
    imshow(dogName2, dogCrop2); // Show our image inside the created window.
    imshow(dogName3, dstImg); // Show our image inside the created window.
    pl(imgPath1);
    pl(imgPath2);
    pl(imgPath3);

    waitKey(0); // Wait for any keystroke in the window
    destroyWindow(dogName1); //destroy the created window
    destroyWindow(dogName2); //destroy the created window
    destroyWindow(dogName3); //destroy the created window
    return 0;
}
