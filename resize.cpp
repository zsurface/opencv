//Uncomment the following line if you are compiling this code in Visual Studio
//#include "stdafx.h"

#include <opencv2/opencv.hpp>
#include <iostream>
#include "AronLib.h"
#include <opencv2/tracking.hpp>

using namespace cv;
using namespace std;
using namespace Utility;

/*
-------------------------------------------------------------------------------- 
Sat Nov 24 13:00:48 2018 
Resize image, create thumbnail image
Compile: runopencv resize.cpp => resize 
Script:  $b/runopencv.sh
-------------------------------------------------------------------------------- [
*/

int main(int argc, char** argv) {
 // Read the image file

 // Mat image(600, 800, CV_8UC3, Scalar(100, 200, 30)); 
    Mat dogImage = imread("/Users/aaa/try/boss2.png");

    Rect roi;

    int cropWidth = 200;
    int cropHeight= 200;
    int offX = (dogImage.size().width  - cropWidth)/2;
    int offY = (dogImage.size().height - cropHeight)/2;
    roi.x = offX;
    roi.y = offY;
    roi.width  = cropWidth; 
    roi.height = cropHeight; 
    float f = 0.003;
    printf("Original Img Width =%d\n", dogImage.size().width);
    printf("Original Img Height=%d\n", dogImage.size().height);
    printf("New Img Width =%d\n", cropWidth);
    printf("New Img Height=%d\n", cropHeight);

    Mat dogCrop = dogImage(roi);

    String dogName = "dog name"; //Name of the window

    String imgPath = "/Users/aaa/try/boss2_resize.png";
    imwrite(imgPath, dogCrop);
    imshow(dogName, dogCrop); // Show our image inside the created window.
    pl(imgPath);

    waitKey(0); // Wait for any keystroke in the window
    destroyWindow(dogName); //destroy the created window
    return 0;
}
