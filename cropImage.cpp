//Uncomment the following line if you are compiling this code in Visual Studio
//#include "stdafx.h"

#include <opencv2/opencv.hpp>
#include <iostream>
#include "AronLib.h"
#include <opencv2/tracking.hpp>

using namespace cv;
using namespace std;
using namespace Utility;

/**
-------------------------------------------------------------------------------- 
Fri Nov 23 17:36:25 2018 
Crop image
Compile: runopencv cropImage  => cropImage 
Script:  $b/runopencv.sh
-------------------------------------------------------------------------------- [
*/

int main(int argc, char** argv) {
 // Read the image file

 // Mat image(600, 800, CV_8UC3, Scalar(100, 200, 30)); 
    Mat dogImage = imread("/Users/aaa/try/bigdog.jpeg");

    Rect roi;

    int cropWidth = 200;
    int cropHeight= 200;
    int offX = (dogImage.size().width  - cropWidth)/2;
    int offY = (dogImage.size().height - cropHeight)/2;
    roi.x = offX;
    roi.y = offY;
    roi.width  = cropWidth; 
    roi.height = cropHeight; 
    float f = 0.003;
    pp("Original Img Width =%d\n", dogImage.size().width);
    pp("Original Img Height=%d\n", dogImage.size().height);
    pp("New Img Width =%d\n", cropWidth);
    pp("New Img Height=%d\n", cropHeight);

    Mat dogCrop = dogImage(roi);

    String dogName = "dog name"; //Name of the window

    String imgPath = "/Users/aaa/try/bigdogcrop.jpeg";
    imwrite(imgPath, dogCrop);
    imshow(dogName, dogCrop); // Show our image inside the created window.
    pl(imgPath);

    waitKey(0); // Wait for any keystroke in the window
    destroyWindow(dogName); //destroy the created window
    return 0;
}
