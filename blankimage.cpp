//Uncomment the following line if you are compiling this code in Visual Studio
//#include "stdafx.h"

#include <opencv2/opencv.hpp>
#include <iostream>
#include "AronLib.h"
#include <opencv2/tracking.hpp>

using namespace cv;
using namespace std;

/*
-------------------------------------------------------------------------------- 
Fri Nov 23 17:36:25 2018 
Draw circle with opencv
Compile: runopencv blankimage.cpp  => blankimage 
Script:  $b/runopencv.sh
-------------------------------------------------------------------------------- [
*/

int main(int argc, char** argv) {
 // Read the image file

 // Mat image(600, 800, CV_8UC3, Scalar(100, 200, 30)); 
 Mat dogImage = imread("/Users/cat/try/dog1.jpeg");
 Mat dog1Image = imread("/Users/cat/try/dog2.jpeg");

 Mat oriImg(600, 800, CV_8UC3, Scalar(100, 200, 30)); 

 const int NUM = 1000;
 float pi = 3.14159;
 float del = 2*pi/NUM;
 float radius = 100;
 // radius is 2
 float arr[NUM][2];
 for(int i=0; i<NUM; i++){
     arr[i][0] = 100 + radius*cos(del*i);
     arr[i][1] = 100 + radius*sin(del*i);
 }

// for(int i=0; i<NUM; i++){
//     int c = abs((int)arr[i][0]);
//     int r = abs((int)arr[i][1]);
//
//     Vec3b color = image.at<Vec3b>(Point(c, r));
//     color[0] = arc4random_uniform(1);
//     color[1] = arc4random_uniform(1);
//     color[2] = 255;
//     image.at<Vec3b>(Point(c, r)) = color;
// }

Rect roi(0, 0, 400, 400);
//roi.width = image.size().width - offWidth;
//roi.height = image.size().height - offHeight;


Mat dogCrop= dogImage(roi);
Mat dog1Crop= dog1Image(roi);

 String dogName = "dog name"; //Name of the window
 String dog1Name = "dog1 name"; //Name of the window

 imwrite("/Users/cat/try/newdog.jpeg", dogCrop);
 imwrite("/Users/cat/try/newdog1.jpeg", dog1Crop);

 imshow(dogName, dogCrop); // Show our image inside the created window.
 imshow(dog1Name,dog1Crop); // Show our image inside the created window.
 
 waitKey(0); // Wait for any keystroke in the window
// destroyWindow(originalName); //destroy the created window
// destroyWindow(modifiedName); //destroy the created window
 destroyWindow(dogName); //destroy the created window
 destroyWindow(dog1Name); //destroy the created window

// Go through each pixel and modify its RBC colors, save the color back to original image
// for(int r=0; r<image.rows; r++){
//     for(int c=0; c<image.cols; c++){
//         Vec3b color = image.at<Vec3b>(Point(c, r));
//         color[0] = arc4random_uniform(10);
//         color[1] = arc4random_uniform(100);
//         color[2] = arc4random_uniform(20);
//         image.at<Vec3b>(Point(c, r)) = color;
//     }
// }

 return 0;
}
