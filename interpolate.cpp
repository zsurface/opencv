//Uncomment the following line if you are compiling this code in Visual Studio
//#include "stdafx.h"

#include <opencv2/opencv.hpp>
#include <iostream>
#include "AronLib.h"
using namespace cv;
using namespace std;
using namespace Utility;

/*
-------------------------------------------------------------------------------- 
Fri Nov 23 17:36:25 2018 
-------------------------------------------------------------------------------- 
Linear interpolate image, enlarge and shrink
Script:  $b/runopencv.sh
Compile: runopencv drawcircle.cpp  => drawcircle 
ref: http://localhost/html/indexOpenCVLinearInterpolation.html
-------------------------------------------------------------------------------- [
*/
int main(int argc, char** argv) {
 // Read the image file

 int factor = 3;
 Mat image = imread("/Users/cat/try/bigdog.jpeg");
 int bigRows = (factor*image.rows) - (factor - 1);
 int bigCols = (factor*image.cols) - (factor - 1);
 Mat bigImg(bigRows, bigCols, CV_8UC3, Scalar(100, 200, 30)); 

 int height= image.size().height;

 //interpolate horizonally
 float t = 1/factor;
 int rr = 0;
 for(int r=0; r<image.rows; r++){
     int cc = 0;
     for(int c=0; c<image.cols; c++){
         // pixel on right edge
         if(c == image.cols - 1){
            Vec3b pt0 = image.at<Vec3b>(Point(c, r));
            Vec3b pt  = bigImg.at<Vec3b>(Point(cc, rr));
            pt[0] = pt0[0];
            pt[1] = pt0[1];
            pt[2] = pt0[2];
            bigImg.at<Vec3b>(Point(cc, rr)) = pt;
            // cc++;
         }else {
            Vec3b pt0 = image.at<Vec3b>(Point(c, r));
            Vec3b pt1 = image.at<Vec3b>(Point(c+1, r));
            for(int k=0; k<factor; k++){
                Vec3b pt = bigImg.at<Vec3b>(Point(cc, rr));
                pt[0] = (1 - k*t)*pt0[0] + k*t*pt1[0];
                pt[1] = (1 - k*t)*pt0[1] + k*t*pt1[1];
                pt[2] = (1 - k*t)*pt0[2] + k*t*pt1[2];
                bigImg.at<Vec3b>(Point(cc, rr)) = pt;
                cc++;
            }
         }
     }
     rr += factor;
 }
 
 // interpolate vertically
for(int c=0; c<bigCols; c++){
    for(int r=0; r<bigRows - (factor - 1); r += factor){
        pp("bigRows=%d, bigCols=%d, c=%d r=%d\n", bigRows, bigCols, c, r);

        Vec3b pt0 = bigImg.at<Vec3b>(Point(c, r));
        Vec3b pt1 = bigImg.at<Vec3b>(Point(c, r+factor));
        for(int k=0; k<factor; k++){
            Vec3b pt = bigImg.at<Vec3b>(Point(c, r + k));
            pt[0] = (1 - k*t)*pt0[0] + k*t*pt1[0];
            pt[1] = (1 - k*t)*pt0[1] + k*t*pt1[1];
            pt[2] = (1 - k*t)*pt0[2] + k*t*pt1[2];
            bigImg.at<Vec3b>(Point(c, r + k)) = pt;
        }
    }
 }

 String originalName = "Original Image"; //Name of the window
 String modifiedName = "Modify Image"; //Name of the window
 namedWindow(originalName); // Create a window
 namedWindow(modifiedName); // Create a window

 imshow(originalName, image); // Show our image inside the created window.
 imshow(modifiedName, bigImg); // Show our image inside the created window.

// image.at<Vec3b>(Point(c, r)) = color;

 waitKey(0); // Wait for any keystroke in the window
 destroyWindow(originalName); //destroy the created window
 destroyWindow(modifiedName); //destroy the created window

 return 0;
}
