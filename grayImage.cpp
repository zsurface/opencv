//Uncomment the following line if you are compiling this code in Visual Studio
//#include "stdafx.h"

#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;

/*
-------------------------------------------------------------------------------- 
Wed Nov 28 16:03:20 2018 
-------------------------------------------------------------------------------- 
open an image only
-------------------------------------------------------------------------------- 
Compile: runopencv loadimage.cpp  => loadimage
Script:  $b/runopencv.sh
-------------------------------------------------------------------------------- [
*/
int main(int argc, char** argv) {
 // Read the image file
 String path = "/Users/cat/myfile/bitbucket/image/dog1.jpeg";
 Mat image = imread(path);
 Mat grayImage; // gray image
 Mat newImage = Mat::zeros(image.size(), image.type());

 // Check for failure
 if (image.empty()) {
  cout << "Could not open or find the image" << endl;
  cout << "Image path=" << path << endl;
  cin.get(); //wait for any key press
  return -1;
 }
 // create gray image
 // cvtColor(image, grayImage, CV_BGR2GRAY);
 cvtColor(image, grayImage, COLOR_BGR2GRAY);

 String imageTitle = "Big dog";
 String grayTitle  = "Gray Image";
 String newTitle   = "Cotrast Brightness";
 
 double alpha = 2.0;
 double beta = 10.0;
 for(int r=0; r<image.rows; r++){
     for(int c=0; c<image.cols; c++){
         for(int i=0; i<3; i++){
            newImage.at<Vec3b>(r, c)[i] = saturate_cast<uchar>( alpha*(image.at<Vec3b>(r, c)[i]) + beta );
         }
     }
 }

 namedWindow(imageTitle); // Create a window
 namedWindow(grayTitle); // Create a window
 namedWindow(newTitle); // Create a window

 imshow(imageTitle, image); // Show our image inside the created window.
 imshow(grayTitle, grayImage); // Show our image inside the created window.
 imshow(newTitle, newImage); // Show our image inside the created window.

 waitKey(0); // Wait for any keystroke in the window

 destroyWindow(imageTitle); //destroy the created window
 destroyWindow(grayTitle); //destroy the created window
 destroyWindow(newTitle); //destroy the created window

 return 0;
}
