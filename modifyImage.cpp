//Uncomment the following line if you are compiling this code in Visual Studio
//#include "stdafx.h"

#include <opencv2/opencv.hpp>
#include <iostream>
#include "AronLib.h"
#include <opencv2/tracking.hpp>

using namespace cv;
using namespace std;

/*
-------------------------------------------------------------------------------- 
Fri Nov 23 17:36:25 2018 
Draw circle with opencv
Compile: runopencv blankimage.cpp  => blankimage 
Script:  $b/runopencv.sh
-------------------------------------------------------------------------------- [
*/

int main(int argc, char** argv) {
 // Read the image file

 Mat dogImage = imread("/Users/cat/try/dog1.jpeg");
 Mat image(dogImage.rows, dogImage.cols, CV_8UC3, Scalar(100, 200, 30)); 

 {
     for(int r=0; r<dogImage.rows; r++){
         for(int c=0; c<dogImage.cols; c++){
             Vec3b color = dogImage.at<Vec3b>(Point(c, r));
             color[0] = color[0] < 125 ? 0 : 255;
             color[1] = color[1] < 125 ? 0 : 255;
             color[2] = color[2] < 125 ? 0 : 255;
             image.at<Vec3b>(Point(c, r)) = color;
         }
     }
 }
 {
     for(int r=0; r<dogImage.rows; r++){
         for(int c=0; c<dogImage.cols; c++){
             Vec3b color = dogImage.at<Vec3b>(Point(c, r));
             color[0] = color[0] == 255 ? 0 : 255;
             color[1] = color[1] == 255 ? 0 : 255;
             color[2] = color[2] == 255 ? 0 : 255;
             image.at<Vec3b>(Point(c, r)) = color;
         }
     }
 }
 {
     for(int r=0; r<dogImage.rows; r++){
         for(int c=0; c<dogImage.cols; c++){
             Vec3b color = dogImage.at<Vec3b>(Point(c, r));
             if(color[0] < 85){
                 color[0] = 0;
             }else if(color[0] >= 85 && color[0] < 170){
                 color[0] = 85; 
             }else{
                 color[0] = 255; 
             }

             if(color[1] < 85){
                 color[1] = 0;
             }else if(color[1] >= 85 && color[1] < 170){
                 color[1] = 85; 
             }else{
                 color[1] = 255; 
             }

             if(color[2] < 85){
                 color[2] = 0;
             }else if(color[2] >= 85 && color[2] < 170){
                 color[2] = 85; 
             }else{
                 color[2] = 255; 
             }
             image.at<Vec3b>(Point(c, r)) = color;
         }
     }
 }

float alpha = 0.5;
float beta = 1 - alpha;
// g(x) = (1 - alpha)f(x) + alpha h(x)
Mat dstImg;
addWeighted(dogImage, alpha, image, beta, 0.0, dstImg);

 String dogName = "dog name"; //Name of the window
 String dogBin  = "0 or 1"; //Name of the window
 String dogBlend  = "blending dog"; //Name of the window
 //imwrite("/Users/cat/try/newdog.jpeg", dogImage);
 imshow(dogName, dogImage); // Show our image inside the created window.
 imshow(dogBin, image); // Show our image inside the created window.
 imshow(dogBlend, dstImg); // Show our image inside the created window.
 imwrite("/Users/cat/try/newdog1.jpeg", image);
 imwrite("/Users/cat/try/newdog2.jpeg", dstImg);
 
 waitKey(0); // Wait for any keystroke in the window
 destroyWindow(dogName); //destroy the created window
 destroyWindow(dogBin); //destroy the created window
 destroyWindow(dogBlend); //destroy the created window

 return 0;
}
