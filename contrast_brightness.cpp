//Uncomment the following line if you are compiling this code in Visual Studio
//#include "stdafx.h"

#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;

/*
-------------------------------------------------------------------------------- 
Wed Nov 28 16:03:20 2018 
-------------------------------------------------------------------------------- 
open an image only
-------------------------------------------------------------------------------- 
Compile: runopencv loadimage.cpp  => loadimage
Script:  $b/runopencv.sh
-------------------------------------------------------------------------------- [
*/
int main(int argc, char** argv) {
 // Read the image file
 String path = "/Users/cat/myfile/bitbucket/image/dog1.jpeg";
 Mat image = imread(path);
 Mat grayImage; // gray image

 // Check for failure
 if (image.empty()) {
  cout << "Could not open or find the image" << endl;
  cout << "Image path=" << path << endl;
  cin.get(); //wait for any key press
  return -1;
 }
 // create gray image
 cvtColor(image, grayImage, CV_BGR2GRAY);

 String imageTitle = "Big dog"; //Name of the window
 String grayTitle = "Gray Image"; //Name of the window

 namedWindow(imageTitle); // Create a window
 namedWindow(grayTitle); // Create a window

 imshow(imageTitle, image); // Show our image inside the created window.
 imshow(grayTitle, grayImage); // Show our image inside the created window.

 waitKey(0); // Wait for any keystroke in the window

 destroyWindow(imageTitle); //destroy the created window
 destroyWindow(grayTitle); //destroy the created window

 return 0;
}
